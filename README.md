Web service for retrieving questions from Question bank
------------------------------------------

To test:

1- Enable web services from admin panel. http://localhost/moodle/admin/settings.php?section=optionalsubsystems

2- clone repo at moodle/mod/

3- rename project folder to wstemplate

4- Install plugin from http://localhost/moodle/admin/index.php

5- Generate webservice token from here : http://localhost/moodle/admin/settings.php?section=webservicetokens

6- Move client folder to php server (www in case of wamp & Documents in case of apache server)

7- run localhost/client/client.php

References
----------

https://docs.moodle.org/dev/Adding_a_web_service_to_a_plugin

https://docs.moodle.org/dev/Question_API

https://docs.moodle.org/dev/Overview_of_the_Moodle_question_engine